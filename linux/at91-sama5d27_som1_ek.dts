/*
 * at91-sama5d27_som1_ek.dts - Device Tree file for SAMA5D27-SOM1-EK board
 *
 *  Copyright (c) 2017, Microchip Technology Inc.
 *                2016 Nicolas Ferre <nicolas.ferre@atmel.com>
 *                2017 Cristian Birsan <cristian.birsan@microchip.com>
 *                2017 Claudiu Beznea <claudiu.beznea@microchip.com>
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */
/dts-v1/;
#include "at91-sama5d27_som1.dtsi"
#include <dt-bindings/mfd/atmel-flexcom.h>
#include <dt-bindings/gpio/gpio.h>

/ {
	model = "Atmel SAMA5D27 SOM1 EK";
	compatible = "atmel,sama5d27-som1-ek", "atmel,sama5d27-som1", "atmel,sama5d27", "atmel,sama5d2", "atmel,sama5";

	aliases {
		serial0 = &uart1;	/* DBGU */
		serial1 = &uart2;	
		serial2 = &uart3;	
		serial3 = &uart4;
		i2c1	= &i2c1;
		i2c2	= &i2c2;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	ahb {
		usb0: gadget@300000 {
			atmel,vbus-gpio = <&pioA PIN_PD20 GPIO_ACTIVE_HIGH>;
			pinctrl-names = "default";
			pinctrl-0 = <&pinctrl_usba_vbus>;
			status = "okay";
		};

		usb1: ohci@400000 {
			num-ports = <3>;
/*			atmel,vbus-gpio = <0 
					   &pioA PIN_PA27 GPIO_ACTIVE_HIGH
					   0
					  >;*/
			atmel,vbus-gpio = <&pioA PIN_PA27 GPIO_ACTIVE_LOW>;
/*			atmel,oc-gpio = <&pioA PIN_PD20 GPIO_ACTIVE_HIGH>;*/
			pinctrl-names = "default";
			pinctrl-0 = <&pinctrl_usb_default>;
			status = "okay";
		};

		usb2: ehci@500000 {
			status = "disabled";
		};

		sdmmc1: sdio-host@b0000000 {
			bus-width = <4>;
			pinctrl-names = "default";
			pinctrl-0 = <&pinctrl_sdmmc1_default>;
			status = "okay";
		};

		apb {
			pmc: pmc@f0014000 {
				pmc_fast_restart {
					compatible = "atmel,sama5d2-pmc-fast-startup";
					#address-cells = <1>;
					#size-cells = <0>;

					atmel,wakeup-rtc-timer;

					wkpin: input@0 {
						reg = <0>;
					};

					gmac_wol: input@10 {
						reg = <10>;
						atmel,wakeup-active-high;
					};
				};
			};

			qspi1: spi@f0024000 {
				status = "okay";
			};

			spi0: spi@f8000000 {
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_spi0_default>;
				status = "disabled";
				spidev@0 {
                                        compatible = "linux,spidev";
                                        spi-max-frequency = <24000000>;
                                        reg = <0>;
                                };

			};

			macb0: ethernet@f8008000 {
				status = "okay";
			};

			tcb0: timer@f800c000 {
				timer0: timer@0 {
					compatible = "atmel,tcb-timer";
					reg = <0>;
				};

				timer1: timer@1 {
					compatible = "atmel,tcb-timer";
					reg = <1>;
				};
			};
/* UARTS */
			uart1: serial@f8020000 {
				atmel,use-dma-rx;
				atmel,use-dma-tx;
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_uart1_default>;
				status = "okay";
			};

			uart2: serial@f8024000 {
				atmel,use-dma-rx;
				atmel,use-dma-tx;
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_uart2_default>;
				status = "okay";
			};

			uart3: serial@fc008000 {
				atmel,use-dma-rx;
				atmel,use-dma-tx;
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_uart3_default>;
				status = "okay";
			};

			uart4: serial@fc00c000 {
				atmel,use-dma-rx;
				atmel,use-dma-tx;
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_uart4_default>;
				status = "okay";
			};

/* FLEXCOMMS */
			flx1: flexcom@f8038000 { /* irDA or i2c extern */
				atmel,flexcom-mode = <ATMEL_FLEXCOM_MODE_TWI>;
				status = "okay";

				i2c2: i2c@600 {
					compatible = "atmel,sama5d2-i2c";
					reg = <0x600 0x200>;
					interrupts = <20 IRQ_TYPE_LEVEL_HIGH 7>;
					dmas = <0>, <0>;
					dma-names = "tx", "rx";
					#address-cells = <1>;
					#size-cells = <0>;
					clocks = <&flx1_clk>;
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx1_default>;
					atmel,fifo-size = <16>;
					status = "okay";
				};
			};

			/* sara g340 with flow-control or external spi */
			flx3: flexcom@fc014000 {
				atmel,flexcom-mode = <ATMEL_FLEXCOM_MODE_SPI>;
				status = "okay";

				uart7: serial@200 { 
					compatible = "atmel,at91sam9260-usart";
					reg = <0x200 0x200>;
					interrupts = <22 IRQ_TYPE_LEVEL_HIGH 7>;
					clocks = <&flx3_clk>;
					clock-names = "usart";
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx3_default>;
					atmel,fifo-size = <32>;
					status = "disabled"; /* uart with flow-control for sara module */
				};

				spi2: spi@400 { 
					compatible = "atmel,at91rm9200-spi";
					reg = <0x400 0x200>;
					interrupts = <22 IRQ_TYPE_LEVEL_HIGH 7>;
					clocks = <&flx3_clk>;
					clock-names = "spi_clk";
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx3_default>;
					atmel,fifo-size = <16>;
					status = "okay"; /* spi on mini-din8 connector */
				};
			};

			/* wifi spi */
			flx4: flexcom@fc018000 {
				atmel,flexcom-mode = <ATMEL_FLEXCOM_MODE_SPI>;
				status = "okay";

				uart6: serial@200 {
					compatible = "atmel,at91sam9260-usart";
					reg = <0x200 0x200>;
					interrupts = <23 IRQ_TYPE_LEVEL_HIGH 7>;
					clocks = <&flx4_clk>;
					clock-names = "usart";
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx4_default>;
					atmel,fifo-size = <32>;
					status = "disabled"; 
				};

				spi3: spi@400 {
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx4_default>;
					status = "okay"; /* spi for atwilc3000 wifi */
					compatible = "atmel,at91rm9200-spi";
					reg = <0x400 0x200>;
					interrupts = <23 IRQ_TYPE_LEVEL_HIGH 7>;
					clocks = <&flx4_clk>;
					clock-names = "spi_clk";
					atmel,fifo-size = <16>;
					wilc_spi@0 {
						compatible = "microchip,wilc1000", "microchip,wilc3000", "atmel,wilc_spi";
						// compatible = "microchip,wilc3000","atmel,wilc_spi";
						spi-max-frequency = <12000000>;
						reg = <0>;
						/*reg = <0x400 0x200>;*/
						reset-gpios =<&pioA PIN_PB25 GPIO_ACTIVE_HIGH>; 
						chip_en-gpios =<&pioA PIN_PB18 GPIO_ACTIVE_HIGH>; 
						irq-gpios = <&pioA PIN_PB23 GPIO_ACTIVE_LOW>;
						interrupt-parent = <&pioA>;
						status = "okay";
					};
				};

				i2c3: i2c@600 {
					compatible = "atmel,sama5d2-i2c";
					reg = <0x600 0x200>;
					interrupts = <23 IRQ_TYPE_LEVEL_HIGH 7>;
					dmas = <0>, <0>;
					dma-names = "tx", "rx";
					#address-cells = <1>;
					#size-cells = <0>;
					clocks = <&flx4_clk>;
					pinctrl-names = "default";
					pinctrl-0 = <&pinctrl_flx4_default>;
					atmel,fifo-size = <16>;
					status = "disabled"; 
				};
			};


			pwm0: pwm@f802c000 {
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_mikrobus1_pwm &pinctrl_mikrobus2_pwm>;
				status = "disabled"; /* Conflict with leds. */
			};


			shdwc@f8048010 {
				atmel,shdwc-debouncer = <976>;
				atmel,wakeup-rtc-timer;

				input@0 {
					reg = <0>;
					atmel,wakeup-type = "low";
				};
			};

			watchdog@f8048040 {
				status = "okay";
			};



			i2c1: i2c@fc028000 {
/*				dmas = <0>, <0>; */
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_i2c1_default>;
				atmel,twd-hold-cycles = <25>; 
				status = "disabled";
				pmic: act8865@5b {
					compatible = "active-semi,act8865";
					reg = <0x5b>;
					active-semi,vsel-low;
					status = "disabled";
					regulators {
						vdd_1v8_reg: DCDC_REG1 {
							regulator-name = "VDD_1V8";
							regulator-min-microvolt = <1800000>;
							regulator-max-microvolt = <1800000>;
							regulator-always-on;
						};
						vdd_1v2_reg: DCDC_REG2 {
							regulator-name = "VDD_1V2";
							regulator-min-microvolt = <1100000>;
							regulator-max-microvolt = <1300000>;
							regulator-always-on;
						};
						vdd_3v3_reg: DCDC_REG3 {
							regulator-name = "VDD_3V3";
							regulator-min-microvolt = <3300000>;
							regulator-max-microvolt = <3300000>;
							regulator-always-on;
						};
						vdd_fuse_reg: LDO_REG1 {
							regulator-name = "VDD_FUSE";
							regulator-min-microvolt = <2500000>;
							regulator-max-microvolt = <2500000>;
							regulator-always-on;
						};
						vdd_3v3_lp_reg: LDO_REG2 {
							regulator-name = "VDD_3V3_LP";
							regulator-min-microvolt = <3300000>;
							regulator-max-microvolt = <3300000>;
							regulator-always-on;
						};
						vdd_led_reg: LDO_REG3 {
							regulator-name = "VDD_LED";
							regulator-min-microvolt = <3300000>;
							regulator-max-microvolt = <3300000>;
						/*	regulator-always-off; */
						};
						vdd_sdhc_1v8_reg: LDO_REG4 {
							regulator-name = "VDD_SDHC_1V8";
							regulator-min-microvolt = <1800000>;
							regulator-max-microvolt = <1800000>;
						/*	regulator-always-off; */
						};
					};
				};
			};

			adc: adc@fc030000 {
				vddana-supply = <&vddana>;
				vref-supply = <&advref>;

				status = "disabled";
			};

			pinctrl@fc038000 {
				pinctrl_can1_default: can1_default {
					pinmux = <PIN_PC26__CANTX1>,
						 <PIN_PC27__CANRX1>;
					bias-disable;
				};

				pinctrl_uart1_default: uart1_default {
					pinmux = <PIN_PD2__URXD1>,
						 <PIN_PD3__UTXD1>;
					bias-disable;
				};

				pinctrl_uart2_default: uart2_default {
					pinmux = <PIN_PD23__URXD2>,
						 <PIN_PD24__UTXD2>;
					bias-disable;
				};

				pinctrl_uart3_default: uart3_default {
					pinmux = <PIN_PC12__URXD3>,
						 <PIN_PC13__UTXD3>;
					bias-disable;
				};

				pinctrl_uart4_default: uart4_default {
					pinmux = <PIN_PB3__URXD4>,
						 <PIN_PB4__UTXD4>;
					bias-disable;
				};

				pinctrl_flx1_default: flx1_default {
					pinmux = <PIN_PA24__FLEXCOM1_IO0>,
						 <PIN_PA23__FLEXCOM1_IO1>;
					bias-disable;
				};

				pinctrl_flx3_default: flx3_default {
					pinmux = <PIN_PC20__FLEXCOM3_IO0>,
						 <PIN_PC19__FLEXCOM3_IO1>,
						 <PIN_PC18__FLEXCOM3_IO2>,
						 <PIN_PC21__FLEXCOM3_IO3>,
						 <PIN_PC22__FLEXCOM3_IO4>;
					bias-disable;
				};

				pinctrl_flx4_default: flx4_default {
					pinmux = <PIN_PC28__FLEXCOM4_IO0>,
						 <PIN_PC29__FLEXCOM4_IO1>,
						 <PIN_PC30__FLEXCOM4_IO2>,
						 <PIN_PC31__FLEXCOM4_IO3>,
						 <PIN_PD0__FLEXCOM4_IO4>;
					bias-disable;
				};


				pinctrl_i2c1_default: i2c1_default {
					pinmux = <PIN_PD4__TWD1>,
						 <PIN_PD5__TWCK1>;
					bias-disable;
				};

				pinctrl_key_gpio_default: key_gpio_default {
					pinmux = <PIN_PA29__GPIO>;
					bias-pull-up;
				};

				pinctrl_led_gpio_default: led_gpio_default {
					pinmux = <PIN_PA10__GPIO>,
						 <PIN_PB1__GPIO>,
						 <PIN_PA31__GPIO>;
					bias-pull-up;
				};

				

				pinctrl_sdmmc1_default: sdmmc1_default {
					cmd_data {
						pinmux = <PIN_PA28__SDMMC1_CMD>,
							 <PIN_PA18__SDMMC1_DAT0>,
							 <PIN_PA19__SDMMC1_DAT1>,
							 <PIN_PA20__SDMMC1_DAT2>,
							 <PIN_PA21__SDMMC1_DAT3>;
						bias-disable;
					};

					conf-ck_cd {
						pinmux = <PIN_PA22__SDMMC1_CK>,
							 <PIN_PA30__SDMMC1_CD>;
						bias-disable;
					};
				};

				pinctrl_spi0_default: spi0_default {
					pinmux = <PIN_PA14__SPI0_SPCK>,
						 <PIN_PA15__SPI0_MOSI>,
						 <PIN_PA16__SPI0_MISO>,
						 <PIN_PA17__SPI0_NPCS0>;
					bias-disable;
				};


				pinctrl_usb_default: usb_default {
					pinmux = <PIN_PA27__GPIO>,
						 <PIN_PD19__GPIO>;
					bias-disable;
				};

				pinctrl_usba_vbus: usba_vbus {
					pinmux = <PIN_PD20__GPIO>;
					bias-disable;
				};

				pinctrl_mikrobus2_rst: mikrobus2_rst {
					pinmux = <PIN_PA26__GPIO>;
					bias-disable;
				};

				pinctrl_mikrobus1_pwm: mikrobus1_pwm {
					pinmux = <PIN_PB1__PWML1>;
					bias-disable;
				};

				pinctrl_mikrobus2_pwm: mikrobus2_pwm {
					pinmux = <PIN_PA31__PWML0>;
					bias-disable;
				};

				pinctrl_mikrobus2_int: mikrobus2_int {
					pinmux = <PIN_PA25__GPIO>;
					bias-disable;
				};


			};

			can1: can@fc050000 {
				pinctrl-names = "default";
				pinctrl-0 = <&pinctrl_can1_default>;
				status = "disabled";
			};
		};
	};

	gpio_keys {
		compatible = "gpio-keys";

		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_key_gpio_default>;

		pb4 {
			label = "USER";
			gpios = <&pioA PIN_PA29 GPIO_ACTIVE_LOW>;
			linux,code = <0x104>;
			wakeup-source;
		};
	};

	leds {
		compatible = "gpio-leds";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_led_gpio_default>;
		status = "okay"; /* Conflict with pwm0. */

		red {
			label = "red";
			gpios = <&pioA PIN_PA10 GPIO_ACTIVE_HIGH>;
		};

		green {
			label = "green";
			gpios = <&pioA PIN_PB1 GPIO_ACTIVE_HIGH>;
		};

		blue {
			label = "blue";
			gpios = <&pioA PIN_PA31 GPIO_ACTIVE_HIGH>;
			linux,default-trigger = "heartbeat";
		};

/*
		 fpga module 
		iccfpga-swd-reset {
			label = "iccfpga-swd-reset";
			gpios = <&pioA PIN_PA11 GPIO_ACTIVE_LOW>;
			default-state = "on"; /* default: reset disabled */
		};

		 fpga module 
		iccfpga-m1-reset {
			label = "iccfpga-m1-reset";
			gpios = <&pioA PIN_PA26 GPIO_ACTIVE_LOW>;
			default-state = "on"; /* default: reset disabled */
		};

		 fpga module power-down 
		iccfpga-power-down {
			label = "iccfpga-power-down";
			gpios = <&pioA PIN_PA3 GPIO_ACTIVE_HIGH>;
			default-state = "on";	/* default: in power-down mode */
		};
*/
		/* atwilc3000 */
/*		atwilc3000-chip-enable {
			label = "atwilc3000-chip-enable";
			gpios = <&pioA PIN_PB18 GPIO_ACTIVE_HIGH>;
			default-value = "on";
		};*/

		/* atwilc3000 */
/*		atwilc3000-reset {
			label = "atwilc3000-reset";
			gpios = <&pioA PIN_PB25 GPIO_ACTIVE_HIGH>;
			default-value = "on";
		};*/
		
	};

	vddin_3v3: fixed-regulator-vddin_3v3 {
		compatible = "regulator-fixed";

		regulator-name = "VDDIN_3V3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
		status = "okay";
	};

	vddana: fixed-regulator-vddana {
		compatible = "regulator-fixed";

		regulator-name = "VDDANA";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&vddin_3v3>;
		status = "okay";
	};

	advref: fixed-regulator-advref {
		compatible = "regulator-fixed";

		regulator-name = "advref";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&vddana>;
		status = "okay";
	};
};

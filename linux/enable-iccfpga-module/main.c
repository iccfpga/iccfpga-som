#include <sys/mman.h>

#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

//*******PIO Controller base memory location**********
#define PIOA ((uint32_t*) (0xFC038000))
#define PIOB ((uint32_t*) (0xFC038000 + 0x40))
#define PIOC ((uint32_t*) (0xFC038000 + 0x80))
#define PIOD ((uint32_t*) (0xFC038000 + 0xc0))


#define PIO_BASE	0xFC038000


#define GPIO_INPUT	0
#define GPIO_OUTPUT	(1<<8)
#define GPIO_PUEN	(1<<9)
#define GPIO_PDEN	(1<<10)
#define GPIO_FUNC_MASK  0x7

#define GPIO_PIO_MSKR	0x00
#define GPIO_PIO_CFGR	0x04
#define GPIO_PIO_PDSR	0x08
#define GPIO_PIO_SODR	0x10
#define GPIO_PIO_CODR	0x14
#define GPIO_PIO_ODSR	0x18

volatile uint32_t* pio = 0;
volatile int gpio_init_done = 0;
	
volatile int fd;

void gpio_write(uint32_t channel, uint32_t offset, uint32_t data) {
	*(pio + ((channel * 0x40 + offset) >> 2)) = data;
}

uint32_t gpio_read(uint32_t channel, uint32_t offset) {
	return *(pio + ((channel * 0x40 + offset) >> 2));
}

void gpio_set_mask(uint32_t channel, uint32_t mask) {
	gpio_write(channel, GPIO_PIO_MSKR, mask);
}

void gpio_set_cfgr(uint32_t channel, uint32_t config) {
	gpio_write(channel, GPIO_PIO_CFGR, config);
}

/*void gpio_conf(uint32_t channel, uint32_t pin, uint32_t config) {
	gpio_set_mask(channel, 1<<pin);
	gpio_set_cfgr(channel, config);
}*/

void gpio_clr_bits(uint32_t channel, uint32_t pins) {
	gpio_write(channel, GPIO_PIO_CODR, pins);
}

void gpio_set_bits(uint32_t channel, uint32_t pins) {
	gpio_write(channel, GPIO_PIO_SODR, pins);
}

uint32_t gpio_get_bits(uint32_t channel) {
	return gpio_read(channel, GPIO_PIO_PDSR);
}
/*
void test() {
	gpio_conf(0, 3, GPIO_OUTPUT);
	gpio_set_bits(0, (1<<3));
}
*/
void gpio_init()
{
	if (gpio_init_done) return;
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd < 0){
		printf("Unable to open /dev/mem");
		return;
	}
	pio = mmap(NULL, 0x100, PROT_READ | PROT_WRITE, MAP_SHARED, fd, PIO_BASE);
	printf("%08x\n", (uint32_t) pio);
	gpio_init_done = 1;
}


int gpio_uninit() {
	int result = 0;
	result = munmap((uint32_t*) pio, 0x100);
        if (result < 0) {
                perror("devmem munmap");
                close(fd);
                return 0;
        }
        close(fd);
	return 1;
}	


#define OUT_RESET_OD    (1<<11)
#define OUT_SWD_CLK     (1<<6)
#define OUT_SWD_DIO     (1<<12)
#define OUT_FPGA_HOLD   (1<<3)
#define OUT_FPGA_RESET  (1<<26)

#define IN_SWD_DIO      (1<<12)
#define PIN_SWD_DIO     12

/* Transition delay coefficients */
static int speed_coeff = 113714;
static int speed_offset = 28;
static unsigned int jtag_delay;


int main()
{
	gpio_init();
    gpio_set_mask(0, OUT_FPGA_HOLD | OUT_FPGA_RESET | OUT_RESET_OD);
    gpio_set_cfgr(0, GPIO_OUTPUT);
    gpio_clr_bits(0, OUT_FPGA_HOLD);
    gpio_set_bits(0, OUT_FPGA_RESET | OUT_RESET_OD);

    
 
	for (int i=0;i<31;i++) {
		gpio_set_mask(0, 1<<i);
		printf("%2i = %08x\n", i, gpio_read(0, 4));
	}
//	gpio_enable_pin(PIO_C, 14);
	printf("Hello world!\n");
	for (int i=0;i<0x40;i+=4) {
		printf("%02x - %08x\n", i, gpio_read(0, i));
	}
	gpio_uninit();
	return 0;
}



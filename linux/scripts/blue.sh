#!/bin/bash

ip -6 addr add 2001:4ca0:0:f000:821f:12ff:fe57:9d1c/64 dev eth0

modprobe wilc_spi
sleep 5
echo BT_POWER_UP > /dev/wilc_bt
#sleep 5
echo BT_FW_CHIP_WAKEUP > /dev/wilc_bt
#sleep 5
echo BT_DOWNLOAD_FW > /dev/wilc_bt
#sleep 10
hciattach /dev/ttyS2 any 115200 noflow
#sleep 5
while (( 1 ))
do
	echo hci0 up ...
	{ hciconfig hci0 up ; } && {
		break
	}
	sleep 5
done

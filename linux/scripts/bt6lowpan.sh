#!/bin/bash

ip -6 addr add 2001:4ca0:0:f000:821f:12ff:fe57:9d1c/64 dev eth0

/etc/init.d/S50radvd start

mount -t debugfs none /sys/kernel/debug

echo 1 > /sys/kernel/debug/bluetooth/6lowpan_enable

echo 1 > /proc/sys/net/ipv6/conf/all/forwarding

timeout -t 4 hcitool lescan

while (( 1 ))
do
	echo "connecting ..."
	echo "connect 00:30:D4:40:66:56 1"  > /sys/kernel/debug/bluetooth/6lowpan_control
	sleep 5

	{ ifconfig | grep "bt0" ; } && {
		break
	}
done

ifconfig bt0 add 2001:db8::1/64

#ifconfig bt0 add 2001:4ca0::0030:d440:6656/64
mosquitto -d

echo
echo "press connect on the sensor"

mosquitto_sub -t "iccfpga/btco2"

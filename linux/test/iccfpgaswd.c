/***************************************************************************
 *   Copyright (C) 2013 by Paul Fertser, fercerpav@gmail.com               *
 *                                                                         *
 *   Copyright (C) 2012 by Creative Product Design, marc @ cpdesign.com.au *
 *   Based on at91rm9200.c (c) Anders Larsen                               *
 *   and RPi GPIO examples by Gert van Loo & Dom                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <jtag/interface.h>
#include "bitbang.h"

#include <sys/mman.h>
#include <stdint.h>

#define PIO_BASE	0xFC038000


#define GPIO_INPUT	0
#define GPIO_OUTPUT	(1<<8)
//#define GPIO_PUEN	(1<<9)
//#define GPIO_PDEN	(1<<10)
//#define GPIO_FUNC_MASK  0x7

#define GPIO_PIO_MSKR	0
#define GPIO_PIO_CFGR	1
#define GPIO_PIO_PDSR	2
#define GPIO_PIO_SODR	4
#define GPIO_PIO_CODR	5
#define GPIO_PIO_ODSR	6


static int devmem_fd;
static uint32_t* pio;

#define OUT_RESET_OD    (1<<11)
#define OUT_SWD_CLK     (1<<6)
#define OUT_SWD_DIO     (1<<12)
#define OUT_FPGA_HOLD   (1<<3)
#define OUT_FPGA_RESET  (1<<26)

#define IN_SWD_DIO      (1<<12)
//#define PIN_SWD_DIO     12

// does nothing because: no jtag^^
static int de10nanoswd_read(void);

// srst enables open-drain reset
static void de10nanoswd_reset(int trst, int srst);

// read swd_dio from input
static int de10nanoswd_swdio_read(void);

// enable driver for swd_dio on output
static void de10nanoswd_swdio_drive(bool is_output);

static int de10nanoswd_init(void);
static int de10nanoswd_quit(void);

// write to swd_dio or swd_clk
static void de10nanoswd_swd_write(int tck, int tms, int tdi);


static struct bitbang_interface de10nanoswd_bitbang = {
	.read = de10nanoswd_read,
	.write = de10nanoswd_swd_write,
	.reset = de10nanoswd_reset,
	.swdio_read = de10nanoswd_swdio_read,
	.swdio_drive = de10nanoswd_swdio_drive,
	.blink = NULL
};

/* Transition delay coefficients */
static int speed_coeff = 300000;//113714;
static int speed_offset = 70;//28;
static unsigned int jtag_delay;


void gpio_write(uint32_t channel, uint32_t offset, uint32_t data) {
	*(pio + channel * 0x10 + offset) = data;
}

uint32_t gpio_read(uint32_t channel, uint32_t offset) {
	return *(pio + channel * 0x10 + offset);
}

void gpio_set_mask(uint32_t channel, uint32_t mask) {
	gpio_write(channel, GPIO_PIO_MSKR, mask);
}

void gpio_set_cfgr(uint32_t channel, uint32_t config) {
	gpio_write(channel, GPIO_PIO_CFGR, config);
}

void gpio_clr_bits(uint32_t channel, uint32_t pins) {
	gpio_write(channel, GPIO_PIO_CODR, pins);
}

void gpio_set_bits(uint32_t channel, uint32_t pins) {
	gpio_write(channel, GPIO_PIO_SODR, pins);
}

uint32_t gpio_read_bits(uint32_t channel, uint32_t pins) {
	return gpio_read(channel, GPIO_PIO_PDSR) & pins;
}


static int de10nanoswd_read(void)
{
    return 0;
}


static void de10nanoswd_swd_write(int tck, int tms, int tdi)
{
        uint32_t state = 0;
        
        state |= (tck) ? OUT_SWD_CLK : 0;
        state |= (tdi) ? OUT_SWD_DIO : 0;

        gpio_set_mask(0, OUT_SWD_CLK | OUT_SWD_DIO);
        gpio_write(0, GPIO_PIO_ODSR, state);

        for (unsigned int i = 0; i < jtag_delay; i++)
                asm volatile ("");
}

/* (1) assert or (0) deassert reset lines */
static void de10nanoswd_reset(int trst, int srst)
{
        if (srst) {
            gpio_clr_bits(0, OUT_RESET_OD);
        } else {
            gpio_set_bits(0, OUT_RESET_OD);
        }
}

static void de10nanoswd_swdio_drive(bool is_output)
{
        if (is_output) {
            gpio_set_mask(0, OUT_SWD_DIO);
            gpio_set_cfgr(0, GPIO_OUTPUT);
        } else {
            gpio_set_mask(0, OUT_SWD_DIO);
            gpio_set_cfgr(0, GPIO_INPUT);
        }
}

static int de10nanoswd_swdio_read(void)
{
    return !!(gpio_read(0, GPIO_PIO_PDSR) & IN_SWD_DIO);
}




static int de10nanoswd_khz(int khz, int *jtag_speed)
{
	if (!khz) {
		LOG_DEBUG("RCLK not supported");
		return ERROR_FAIL;
	}
	*jtag_speed = speed_coeff/khz - speed_offset;
	if (*jtag_speed < 0)
		*jtag_speed = 0;
	return ERROR_OK;
}

static int de10nanoswd_speed_div(int speed, int *khz)
{
	*khz = speed_coeff/(speed + speed_offset);
	return ERROR_OK;
}

static int de10nanoswd_speed(int speed)
{
	jtag_delay = speed;
	return ERROR_OK;
}

COMMAND_HANDLER(de10nanoswd_handle_speed_coeffs)
{
	if (CMD_ARGC == 2) {
		COMMAND_PARSE_NUMBER(int, CMD_ARGV[0], speed_coeff);
		COMMAND_PARSE_NUMBER(int, CMD_ARGV[1], speed_offset);
	}
	return ERROR_OK;
}

static const struct command_registration de10nanoswd_command_handlers[] = {
	{
		.name = "de10nanoswd_speed_coeffs",
		.handler = &de10nanoswd_handle_speed_coeffs,
		.mode = COMMAND_CONFIG,
		.help = "SPEED_COEFF and SPEED_OFFSET for delay calculations.",
	},
	COMMAND_REGISTRATION_DONE
};

static const char * const de10nanoswd_transports[] = { "swd", NULL };

struct jtag_interface de10nanoswd_interface = {
	.name = "de10nanoswd",
	.supported = DEBUG_CAP_TMS_SEQ,
	.execute_queue = bitbang_execute_queue,
	.transports = de10nanoswd_transports,
	.swd = &bitbang_swd,
	.speed = de10nanoswd_speed,
	.khz = de10nanoswd_khz,
	.speed_div = de10nanoswd_speed_div,
	.commands = de10nanoswd_command_handlers,
	.init = de10nanoswd_init,
	.quit = de10nanoswd_quit,
};

static int de10nanoswd_init(void)
{
	bitbang_interface = &de10nanoswd_bitbang;

	LOG_INFO("DE10NANOSWD GPIO SWD bitbang driver");

// Open up the /dev/mem device (aka, RAM)
	devmem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (devmem_fd < 0) {
		perror("devmem open");
		return ERROR_JTAG_INIT_FAILED;
	}
	// mmap() the entire address space of the Lightweight bridge so we can access our custom module
	pio = (uint32_t*) mmap(NULL, 0x100, PROT_READ | PROT_WRITE, MAP_SHARED, devmem_fd, PIO_BASE);
	if (pio == MAP_FAILED) {
		perror("devmem mmap");
		close(devmem_fd);
		return ERROR_JTAG_INIT_FAILED;
	}


    gpio_set_mask(0, OUT_FPGA_HOLD | OUT_FPGA_RESET);
    gpio_set_cfgr(0, GPIO_OUTPUT);
    
    gpio_set_bits(0, OUT_FPGA_RESET);
    gpio_clr_bits(0, OUT_FPGA_HOLD);
	
	/*
	 * Configure TDO as an input, and TDI, TCK, TMS, TRST, SRST
	 * as outputs.  Drive TDI and TCK low, and TMS/TRST/SRST high.
	 */

    gpio_clr_bits(0, OUT_SWD_DIO | OUT_SWD_CLK);

    gpio_set_mask(0, OUT_SWD_DIO | OUT_SWD_CLK);
    gpio_set_cfgr(0, GPIO_OUTPUT);


    // clr reset
    gpio_clr_bits(0, OUT_RESET_OD);

    // reset to output
    gpio_set_mask(0, OUT_RESET_OD); // TODO?
    gpio_set_cfgr(0, GPIO_OUTPUT);  // open-drain
    
	bitbang_switch_to_swd();
	return ERROR_OK;
}

static int de10nanoswd_quit(void)
{
	// disable swd completly
    gpio_set_mask(0, OUT_RESET_OD | OUT_SWD_DIO | OUT_SWD_CLK);
    gpio_set_cfgr(0, GPIO_INPUT);

	int result = 0;
	result = munmap(pio, 0x100);
	if (result < 0) {
		perror("devmem munmap");
		close(devmem_fd);
		return !ERROR_OK;
	}
	close(devmem_fd);


	return ERROR_OK;
}
